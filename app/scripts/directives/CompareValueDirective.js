'use strict';
angular.module('bApp').directive('compareTo', function($timeout, $parse) {
    return {
        require: 'ngModel',
        link: function(scope, element, attributes, ngModel) {

            var compareTo = function(modelValue) {
                var otherModelValue = $parse(attributes.compareTo)(scope);
                if (modelValue && otherModelValue) {
                    var comparisonType = attributes.compareType;
                    if (attributes.compareDataType === 'date') {
                        modelValue = new Date(modelValue).getTime();
                        otherModelValue = new Date(otherModelValue).getTime();
                    }
                    if (comparisonType === 'equals') {
                        ngModel.$setValidity('compareTo', modelValue === otherModelValue);
                    } else if (comparisonType === 'less') {
                        ngModel.$setValidity('compareTo', modelValue < otherModelValue);
                    } else if (comparisonType === 'more') {
                        ngModel.$setValidity('compareTo', modelValue > otherModelValue);
                    } else {
                        ngModel.$setValidity('compareTo', modelValue !== otherModelValue);
                    }

                }
                return modelValue;
            };
            ngModel.$parsers.push(compareTo);


        }
    }
});
