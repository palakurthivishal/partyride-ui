'use strict';
angular.module('bApp').controller('AddEventController', function($scope, $state, $q, $rootScope,AppContext, toastr, StorageService, AjaxService, Upload) {
    $scope.mode = 'add';
    $scope.ev = {
        startDate : moment(new Date().getTime()).format('DD MMM YYYY HH:mm'),
        endDate : moment(new Date().getTime() + 864000).format('DD MMM YYYY HH:mm')
    };
    $scope.submit = function() {
        var ev = angular.copy($scope.ev);

        var payload = {
            name: ev.name,
            description: ev.description,
            standardPrice: ev.standardPrice,
            discountPrice: ev.discountPrice,
            tagName: ev.tagName,
            tagDescription: ev.tagDescription,
            startDate: new Date(ev.startDate).getTime(),
            endDate: new Date(ev.endDate).getTime(),
            pubImageVOs: []
        };
        $scope.uploadPics(payload);

    };
    $scope.uploadPics = function(eventPayload) {
        var eventPayload = eventPayload;
        var images = $scope.ev.images;
        if (!(images instanceof Array)) {
            images = [images];
        }
        var imageReqs = [];
        images.forEach(function(image, i) {
            var index = i;
            var fileUpload = Upload.upload({
                url: AppContext.CLOUDINARY_URL,
                fields: {
                    upload_preset: AppContext.CLOUDINARY_PRESET,
                    tags: AppContext.CLOUDINARY_TAG,
                    context: 'photo=test'
                },
                file: image
            });
            imageReqs.push(fileUpload);
        });
        $q.all(imageReqs).then(function(imageArr) {
            var files = [];
            imageArr.forEach(function(d) {
                files.push({
                    original_filename: d.data.original_filename,
                    url: d.data.url,
                    public_id: d.data.public_id,
                    format: d.data.format,
                    created_at: d.data.created_at,
                });
            });
            eventPayload.pubImageVOs = files;
            AjaxService.hit('pubs/' + $rootScope.userData.pubId + '/events', 'POST', eventPayload).success(function() {
                toastr.success('Event is created for this timeline');
                $state.reload();
            }).error(function() {
                toastr.error('Event creation failed');
            });

        });
    };
});
