'use strict';
angular.module('bApp').controller('ChangePasswordController', function($scope, $rootScope, $state, toastr, AjaxService) {
    $scope.submit = function() {
        var payload = {
            oldPwd: $scope.oldPwd,
            newPwd: $scope.newPwd
        };
        AjaxService.hit('users/' + $rootScope.userData.userId + '/updatepwd', 'POST', payload).success(function() {
            toastr.success('Password has been changed, Please login to continue');
            $rootScope.$broadcast('LOGGED_OUT');
        }).error(function(resp) {
            toastr.error(resp.error);
        });
    };
});
