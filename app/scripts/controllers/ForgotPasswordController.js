'use strict';
angular.module('bApp').controller('ForgotPasswordController', function($scope, toastr, AjaxService) {
    $scope.submit = function() {
        AjaxService.hit('users/resetPwd', 'POST', $scope.email).success(function() {
        	$scope.email = '';
            toastr.success('Passowrd has been sent to the registered email address');
        }).error(function(resp) {
            toastr.error(resp.error);
        });
    };
});
