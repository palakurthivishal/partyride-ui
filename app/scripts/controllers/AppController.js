'use strict';
angular.module('bApp').controller('AppController', function($scope,$state,$timeout, $rootScope, StorageService) {
    
    $scope.logout = function(){
    	StorageService.clearAll();
    	$rootScope.userData = null;
    	$state.go('login',{},{reload : true});
    };
    $scope.goBackToPrevState = function(){
        $rootScope.screen.transition = 'right';
        window.history.back();
        $timeout(function(){
            $rootScope.screen.transition = 'left';
        },0);
    };

    $rootScope.$on('LOGGED_IN', function() {
        $rootScope.setEnvironment();
    });
    $rootScope.$on('LOGGED_OUT', function() {
        $scope.logout();
    });

    $rootScope.$on('$stateChangeStart', function(ev, to, from) {
        var isLoggedIn = $rootScope.isLoggedIn();
        if (isLoggedIn) {
            if (to.noAuth) {
                ev.preventDefault();
                return;
            }
        } else {
            if (!to.noAuth) {
                ev.preventDefault();
                $state.go('login');
                return;
            }
        }
        $rootScope.screen.title = to.title;
        $rootScope.screen.back = to.back;
        $rootScope.screen.menu = to.menu;
    });
});
