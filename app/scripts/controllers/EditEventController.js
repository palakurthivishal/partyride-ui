'use strict';
angular.module('bApp').controller('EditEventController', function($scope, $state, $rootScope, blockUI, toastr, AjaxService) {
    $scope.init = function() {
        if ($scope.selectedEvent) {
            blockUI.start();
            $scope.selectedEvent.startDate = moment($scope.selectedEvent.startDate).format('DD MMM YYYY HH:mm');
            $scope.selectedEvent.endDate = moment($scope.selectedEvent.endDate).format('DD MMM YYYY HH:mm');
            $scope.ev = angular.copy($scope.selectedEvent);
            $scope.ev.discount = $scope.ev.discountPrice ? true : false;
            $scope.mode = 'edit';
            blockUI.stop();
        } else {
            $state.go('event.add');
            return;
        }
    };
    $scope.submit = function() {
        var ev = angular.copy($scope.ev);
        var payload = {
            name: ev.name,
            description: ev.description,
            standardPrice: ev.standardPrice,
            discountPrice: ev.discount ? ev.discountPrice : null,
            tagName: ev.discount ? ev.tagName : null,
            tagDescription: ev.discount ? ev.tagDescription : null,
            startDate: new Date(ev.startDate).getTime(),
            endDate: new Date(ev.endDate).getTime(),
            pubImageVOs: ev.pubImageVOs
        };
        AjaxService.hit('pubs/' + $rootScope.userData.pubId + '/events/' + ev.eventId, 'PUT', payload).success(function() {
            toastr.success('Event has been updated');
            $state.reload();
        }).error(function() {
            toastr.error('Event updation failed');
        });
    };
    $scope.init();
    $scope.$watch('selectedEvent', function(newVal) {
        $scope.init();
    });
})
