'use strict';
angular.module('bApp').controller('EventsController', function($scope, $rootScope, $state, toastr, AjaxService) {
    $scope.addEvent = function() {
        $state.go('event.add');
    };
    $scope.editEvent = function(event, index) {
        $scope.selectedEvent = event;
        $state.go('event.edit');
    };
    $scope.viewEvent = function(event, index) {
        $scope.selectedEvent = event;
        $state.go('event.detail');
    };
    $scope.deleteEvent = function(event, index) {
        AjaxService.hit('pubs/' + $rootScope.userData.pubId + '/events/' + event.eventId, 'DELETE').success(function() {
            toastr.success('Event deleted succesfully');
            $state.reload();
        }).error(function(resp){
            toastr.error(resp.error);
        });
    };
    $scope.getEvents = function() {
        AjaxService.hit('pubs/' + $rootScope.userData.pubId + '/events', 'GET').success(function(resp) { //$rootScope.userData.pubId
            var events = resp.data;
            $scope.events = events;
        }).error(function() {});
    };
    $scope.getEvents();
});
