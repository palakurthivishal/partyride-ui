'use strict';
angular.module('bApp').controller('RegistrationController', function($scope, $q, $rootScope, $state, $timeout, AjaxService, toastr, Upload, GeoLocation, AppContext) {
    var geolocation = $rootScope.geoLocation;
    $scope.map = {
        center: {
            latitude: geolocation ? $rootScope.geoLocation.coords.latitude : 17,
            longitude: geolocation ? $rootScope.geoLocation.coords.longitude : 78
        },
        zoom: 14
    };
    $scope.marker = {
        id: 0,
        coords: {
            latitude: geolocation ? $rootScope.geoLocation.coords.latitude : 17,
            longitude: geolocation ? $rootScope.geoLocation.coords.longitude : 78
        },
        options: {
            draggable: true,
            icon: 'images/pub_marker.png'
        }
    };
    $scope.register = function() {
        if ($scope.registrationForm.$invalid) {
            return;
        }
        var payload = {
            pubName: $scope.pub.pubName,
            userName: $scope.pub.email,
            email: $scope.pub.email,
            address: $scope.pub.address,
            password: $scope.pub.password,
            latitude: $scope.marker.coords.latitude,
            longitude: $scope.marker.coords.longitude,
            pubImageVOs: []
        };
        AjaxService.hit('pubs', 'POST', payload).success(function(resp) {
            toastr.success('Pub has been registered succesfully. Please login to continue');
            $scope.uploadPics(payload, resp);
        }).error(function(resp, statusCode) {
            if (statusCode === 409)
                toastr.error('User with this email address already exists');
            else
                toastr.error(resp.error);
        });

    };
    $scope.uploadPics = function(pubVO, pubResp) {
        var pubPayload = pubVO;
        var pubResp = pubResp;
        var images = $scope.pub.images;
        if (!(images instanceof Array)) {
            images = [images];
        }
        var imageReqs = [];
        images.forEach(function(image, i) {
            var index = i;
            var fileUpload = Upload.upload({
                url: AppContext.CLOUDINARY_URL,
                fields: {
                    upload_preset: AppContext.CLOUDINARY_PRESET,
                    tags: AppContext.CLOUDINARY_TAG,
                    context: 'photo=test'
                },
                file: image
            });
            imageReqs.push(fileUpload);
        });
        $q.all(imageReqs).then(function(imageArr) {
            var files = [];
            imageArr.forEach(function(d) {
                files.push({
                    original_filename: d.data.original_filename,
                    url: d.data.url,
                    public_id: d.data.public_id,
                    format: d.data.format,
                    created_at: d.data.created_at,
                });
            });
            pubPayload.pubImageVOs = files;
            var updatePub = AjaxService.hit('pubs/' + pubResp.data.pubId, 'PUT', pubPayload);
            updatePub.success(function() {
                $state.go('login');
            }).error(function() {
                toastr.error('Images upload failed');
                $state.go('login');
            });

        });
    };

    $rootScope.$on('TRACKED_LOCATION', function(ev, data) {
        $timeout(function() {
            $scope.map = {
                center: {
                    latitude: $rootScope.geoLocation.coords.latitude, //GeoLocation.geo().coords.latitude,
                    longitude: $rootScope.geoLocation.coords.longitude //GeoLocation.geo().coords.longitude
                },
                zoom: 14
            };
            $scope.marker = {
                id: 0,
                coords: {
                    latitude: $rootScope.geoLocation.coords.latitude, //GeoLocation.geo().coords.latitude,
                    longitude: $rootScope.geoLocation.coords.longitude //GeoLocation.geo().coords.longitude
                },
                options: {
                    draggable: true,
                    icon: 'images/pub_marker.png'
                }
            };
        }, 0)
    });

});
