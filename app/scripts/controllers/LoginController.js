'use strict';
angular.module('bApp').controller('LoginController', function($scope, $rootScope, $state, toastr, AjaxService, Upload, StorageService) {
    $scope.login = function() {

        if ($scope.loginForm.$invalid) {
            return;
        }
        var payload = {
            username: $scope.user.username,
            password: $scope.user.password
        }
        AjaxService.hit('login', 'POST', $.param(payload), {
            'Content-Type': 'application/x-www-form-urlencoded'
        }).success(function(resp) {
            StorageService.clearAll();
            StorageService.set('USER_ID', resp.data.userId);
            StorageService.set('PUB_ID', resp.data.pubId);
            //$state.go('event.add');
            $state.go('dashboard');
            $rootScope.$broadcast("LOGGED_IN");
        }).error(function() {
            toastr.error('Please recheck your username and password');
        });
    };
});
