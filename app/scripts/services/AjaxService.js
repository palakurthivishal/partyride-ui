'use strict';
angular.module('bApp').service('AjaxService', function($http,toastr) {
    var _basePath = 'http://partyride-partyridenaresh.rhcloud.com/partyride-0.0.1-SNAPSHOT/';
    var _defaultHeaders = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    };

    var _mergeObjects = function(obj1, obj2) {
        var obj3 = {};
        for (var attrname in obj1) {
            obj3[attrname] = obj1[attrname];
        }
        for (var attrname in obj2) {
            obj3[attrname] = obj2[attrname];
        }
        return obj3;
    }
    return {
        hit: function(url, method, payload, optHeaderObj) {
            var promise = $http({
                url: _basePath + url,
                method: method,
                data: payload,
                headers: optHeaderObj ? _mergeObjects(_defaultHeaders, optHeaderObj) : _defaultHeaders
            });
            promise.error(function(resp){
            	// toastr.error(resp.message);
            });
            return promise;
        }
    }
});
