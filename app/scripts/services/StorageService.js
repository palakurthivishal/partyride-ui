'use strict';
angular.module('bApp').service('StorageService', function() {
    var set = function(key, value) {
        window.sessionStorage.setItem(key, value);
    };
    var get = function(key) {
        return window.sessionStorage.getItem(key);
    };
    var clearAll = function(){
    	window.sessionStorage.clear();
    };
    return {
        get: get,
        set: set,
        clearAll : clearAll
    };
});
