'use strict';
angular.module('bApp').filter('moment', function($sce) {
    return function(time, timeFormat) {
        if (time == undefined) {
            return;
        } else {
        	timeFormat = timeFormat ? timeFormat : 'DD MMM YYYY, HH:mm';
            return moment(time).format(timeFormat);
        }
        // return $sce.trustAsHtml(txt);
    };
});
