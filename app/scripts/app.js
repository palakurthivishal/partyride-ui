'use strict';

/**
 * @ngdoc overview
 * @name bApp
 * @description
 * # bApp
 *
 * Main module of the application.
 */
/*
,
       'uiGmapgoogle-maps',
       'toastr',
       'cloudinary',
       'blockUI',
       */
angular
    .module('bApp', [
        'cloudinary',
        'whimsicalRipple',
        'toastr',
        'blockUI',
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngMessages',
        'ngTouch',
        'ui.router',
        'anim-in-out',
        'azirkoDatetimepicker',
        'ngFileUpload',
        'uiGmapgoogle-maps',
        'ui.bootstrap'
    ])
    .config(function($stateProvider, $urlRouterProvider, toastrConfig, blockUIConfig, uiGmapGoogleMapApiProvider) {
        uiGmapGoogleMapApiProvider.configure({
            //    key: 'your api key',
            v: '3.20', //defaults to latest 3.X anyhow
            libraries: 'places' // Required for SearchBox.
        });
        blockUIConfig.message = 'Please wait';
        blockUIConfig.template = '<div class=\"block-ui-overlay\"></div><div class=\"block-ui-message-container\" aria-live=\"assertive\" aria-atomic=\"true\"><div class=\"block-ui-message\" ng-class=\"$_blockUiMessageClass\"><small><i class="fa fa-spinner fa-spin"></i>&nbsp;{{ state.message }}</small></div></div>';
        angular.extend(toastrConfig, {
            containerId: 'toast-container',
            maxOpened: 0,
            newestOnTop: true,
            tapToDismiss: false,
            allowHtml: true,
            closeButton: true,
            closeHtml: '<button>&times;</button>',
            positionClass: 'toast-top-right',
            preventDuplicates: false,
            preventOpenDuplicates: false,
            target: 'body'
        });
        $stateProvider.state('home', {
            url: '/home',
            controller: 'HomeController',
            templateUrl: 'views/homeView.html',
            title: '',
            noAuth: true,
            back: false,
            menu: false,
        }).state('login', {
            url: '/login',
            controller: 'LoginController',
            templateUrl: 'views/loginView.html',
            title: 'Login',
            noAuth: true,
            back: false,
            menu: false,
        }).state('register', {
            url: '/register',
            controller: 'RegistrationController',
            templateUrl: 'views/registrationView.html',
            title: 'Registration',
            back: false,
            noAuth: true,
            menu: true
        }).state('forgotPassword', {
            url: '/forgotPassword',
            controller: 'ForgotPasswordController',
            templateUrl: 'views/forgotPasswordView.html',
            title: 'Forgot Password',
            back: false,
            noAuth: true,
            menu: true
        }).state('changePassword', {
            url: '/changePassword',
            controller: 'ChangePasswordController',
            templateUrl: 'views/changePasswordView.html',
            title: 'Change Password',
            back: true,
            menu: true
        }).state('dashboard', {
            url: '/dashboard',
            controller: 'DashboardController',
            templateUrl: 'views/dashboardView.html',
            title: 'Dashboard',
            menu: true
        }).state('event', {
            url: '/event',
            controller: 'EventsController',
            templateUrl: 'views/eventsView.html',
            title: 'Events',
            abstract: true,
            menu: true
        }).state('event.add', {
            url: '/add',
            controller: 'AddEventController',
            templateUrl: 'views/eventView.html',
            title: 'Events',
            reload: true,
            menu: true
        }).state('event.detail', {
            url: '/detail',
            controller: 'EditEventController',
            templateUrl: 'views/eventDetailView.html',
            title: 'Events',
            reload: true,
            menu: true
        }).state('event.edit', {
            url: '/edit',
            controller: 'EditEventController',
            templateUrl: 'views/eventView.html',
            title: 'Events',
            reload: true,
            menu: true
        });
        $urlRouterProvider.otherwise('/login');
    }).run(function($state, $rootScope, StorageService) {
        /* Date display format */
        moment.defaultFormat = 'DD/MM/YYYY, HH:mm';
        $rootScope.cloudinary = {
            url: 'https://api.cloudinary.com/v1_1/palakurthivishal/upload',
            preset: 'ktb6rnm2'
        };

        $rootScope.screen = {
            transition : 'left'
        };
        $rootScope.isLoggedIn = function() {
            var userId = StorageService.get('USER_ID');
            return userId ? true : false;
        };
        $rootScope.setEnvironment = function() {
            if ($rootScope.isLoggedIn()) {
                $rootScope.userData = {
                    userId: StorageService.get('USER_ID'),
                    pubId: StorageService.get('PUB_ID')
                };
            }
        };
        $rootScope.setEnvironment();
    });
