'use strict';
angular.module('bApp').service('GeoLocation', function($rootScope) {
    var geolocation = {};
    geolocation.get = function() {
        var map = {};
        var location = navigator.geolocation;
        location.getCurrentPosition(function(resp) {
        	$rootScope.geoLocation = resp;
            $rootScope.$broadcast('TRACKED_LOCATION');
        });
        return {};
    }
    geolocation.get();
    return geolocation;
});
